import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import axios from "axios";

import Login from "./components/auth/Login";
import AddPostcode from "./components/auth/AddPostcode";
import GetCurrentUser from "./components/auth/GetCurrentUser";
import ChangePassword from "./components/auth/ChangePassword";
import Logout from "./components/auth/Logout";
import RefreshToken from "./components/auth/RefreshToken";
import Register from "./components/auth/Register";

import MyOrders from "./components/order/MyOrders";
import ReorderAnOrder from "./components/order/ReorderAnOrder";
import MyAdresses from "./components/order/MyAdresses";
import AddAdress from "./components/order/AddAdress";
import GetCategories from "./components/order/GetCategories";
import GetProducts from "./components/order/GetProducts";
import CreateOrder from "./components/order/CreateOrder";

function App() {
  const [token, setToken] = useState("");

  const getCategories = () => {
    setToken(
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL2J1cmdlcnlcL3B1YmxpY1wvYXBpXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU4OTM5MTM2NSwiZXhwIjoxNTkxOTgzMzY1LCJuYmYiOjE1ODkzOTEzNjUsImp0aSI6InpjbnRYYkZYVUViZVo5WGoiLCJzdWIiOjEsInBydiI6IjM3ODdmYmExNjE4YTkzMDUyNmFjYTZjOGJiOWI0NGI4M2YyOTc3MjYifQ.z7nNuOZRVOYUVeG6TRJKH5vzv1Q3aXbXLvUr3XpPdes"
    );

    console.log(token);
  };

  return (
    <div className="App">
      <br />
      Token: {token ? token : "nu esti autentificat"}
      <br />
      <br />
      <hr />
      <Login token={token} setToken={setToken} />
      <br />
      <br />
      <Register token={token} setToken={setToken} />
      <br />
      <br />
      <AddPostcode token={token} setToken={setToken} />
      <br />
      <br />
      <GetCurrentUser token={token} />
      <br />
      <br />
      <ChangePassword token={token} />
      <br />
      <br />
      <Logout token={token} setToken={setToken} />
      <br />
      <br />
      <RefreshToken token={token} setToken={setToken} />
      <br />
      <br />
      <MyOrders token={token} />
      <br />
      <br />
      <ReorderAnOrder token={token} />
      <br />
      <br />
      <MyAdresses token={token} />
      <br />
      <br />
      <AddAdress token={token} />
      <br />
      <br />
      <hr />
      <br />
      <GetCategories token={token} />
      <br />
      <br />
      <GetProducts token={token} />
      <br />
      <br />
      <CreateOrder token={token} />
    </div>
  );
}

export default App;
