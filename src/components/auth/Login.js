import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const Login = (props) => {
  const sendRequest = () => {
    console.log("send login");
    // Momentan este doar un cont de client cu datele de mai jos.
    // Poti sa il folosesti pe asta sau sa iti faci altul pe ruta de register
    axios
      .post(data.baseUrl + "api/auth/login", {
        email: "client@yahoo.com",
        password: "password",
      })
      .then((res) => {
        console.log(res.data);
        const newToken = res.data.access_token;
        props.setToken(newToken);
      });
  };

  return <button onClick={sendRequest}>Request ruta login</button>;
};

export default Login;
