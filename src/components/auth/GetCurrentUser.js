import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const GetCurrentUser = (props) => {
  const sendRequest = () => {
    console.log("get current user");
    // Obtine userul autentificat din token-ul primit
    axios
      .post(data.baseUrl + "api/auth/me", {
        token: props.token,
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Userul autentificat</button>;
};

export default GetCurrentUser;
