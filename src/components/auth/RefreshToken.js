import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const RefreshToken = (props) => {
  const sendRequest = () => {
    console.log("refresh token");
    axios
      .post(data.baseUrl + "api/auth/refresh", {
        token: props.token,
      })
      .then((res) => {
        console.log(res.data);
        props.setToken(res.data.access_token);
      });
  };

  return <button onClick={sendRequest}>Refresh token (optional)</button>;
};

export default RefreshToken;
