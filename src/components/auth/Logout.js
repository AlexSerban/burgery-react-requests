import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const Logout = (props) => {
  const sendRequest = () => {
    console.log("logout user");
    axios
      .post(data.baseUrl + "api/auth/logout", {
        token: props.token,
      })
      .then((res) => {
        console.log(res.data);
        props.setToken("");
      });
  };

  return <button onClick={sendRequest}>Logout / Stergere token</button>;
};

export default Logout;
