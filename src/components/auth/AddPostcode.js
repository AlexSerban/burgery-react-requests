import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const AddPostcode = (props) => {
  const sendRequest = () => {
    console.log("add postcode");
    // Cu token-ul primit la logare / inregistrare poti poti sa modifici codul postal al clientului respectiv
    axios
      .post(data.baseUrl + "api/post_code", {
        // token client
        token: props.token,
        // cod postal dorit
        postcode: 1234,
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Request ruta postcode</button>;
};

export default AddPostcode;
