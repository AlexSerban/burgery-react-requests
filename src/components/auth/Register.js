import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const Register = (props) => {
  const sendRequest = () => {
    console.log("send register");
    // Creaza user nou. Exista niste validari facute pe backend spre exemplu nu ai voie
    // sa faci 2 useri cu acelasi email.
    // Dupa ce creezi userul ar trebui sa te duci la screenul de add postcode. Pentru acela vei folosi ruta de add postcode
    axios
      .post(data.baseUrl + "api/auth/register", {
        email: "user1@yahoo.com",
        password: "password",
        name: "Alex",
        last_name: "Serban",
        // ceva datepicker
        birthdate: new Date(),
        // din dropdown
        prefix: "+40",
        phone: "0202020202020",
      })
      .then((res) => {
        console.log(res.data);
        const newToken = res.data.access_token;
        props.setToken(newToken);
      });
  };

  return <button onClick={sendRequest}>Request ruta register</button>;
};

export default Register;
