import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const ChangePassword = (props) => {
  const sendRequest = () => {
    console.log("get current user");
    // Schimba parola
    axios
      .post(data.baseUrl + "api/auth/change_password", {
        token: props.token,
        // current password
        password: "password",
        // new password
        new_password: "password",
        // repeat new password
        repeat_password: "password",
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Modifica parola</button>;
};

export default ChangePassword;
