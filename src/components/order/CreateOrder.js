import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const CreateOrder = (props) => {
  const sendRequest = () => {
    console.log("Add new adress");
    // Creaza o comanda noua. Produsele le vei avea intr-un array salvat intr-un state
    // care se va modifica de fiecare data cand adaugi un produs in cart
    // array ul ar trebui sa arate ceva de genul
    const products_to_send = [
      {
        // id produs adaugat
        product_id: 3,
        // cantitate
        quantity: 1,
      },
      {
        // id produs adaugat
        product_id: 2,
        // cantitate
        quantity: 1,
      },
    ];
    // adresa selectata la checkout trebuie trimisa. Mai exact trimiti id-ul
    const adress_id = 1;

    // tipul comenzii. 1 - eat in / 2 - take away
    const order_type = 1;

    // timpul de livrare. Daca trimiti 0 inseamna cat de repede posibil. Daca nu trimite sub formatul 10:45 sau cum vrei
    const delivery_time = 0;

    axios
      .post(data.baseUrl + "api/create_order", {
        token: props.token,
        products: products_to_send,
        adress_id: adress_id,
        order_type: order_type,
        delivery_time: delivery_time,
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Trimite comanda</button>;
};

export default CreateOrder;
