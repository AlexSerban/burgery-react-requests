import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const MyAdresses = (props) => {
  const sendRequest = () => {
    console.log("get current user's adresses");
    // Obtine adresele userului autentificat
    axios
      .get(data.baseUrl + "api/auth/my_adresses", {
        params: {
          token: props.token,
        },
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Adresele userului</button>;
};

export default MyAdresses;
