import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const GetProducts = (props) => {
  const sendRequest = () => {
    console.log("get categories");
    // Obtine produsele din categoria cu id-ul category_id
    const category_id = 1;

    axios
      .get(data.baseUrl + "api/category/" + category_id + "/products")
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Get PRODUCT IS A CATEGORY</button>;
};

export default GetProducts;
