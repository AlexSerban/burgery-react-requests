import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const MyOrders = (props) => {
  const sendRequest = () => {
    console.log("get current user's orders");
    // Obtine comenzile userului autentificat
    axios
      .get(data.baseUrl + "api/auth/my_orders", {
        params: {
          token: props.token,
        },
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Comenzi user autentificat</button>;
};

export default MyOrders;
