import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const AddAdress = (props) => {
  const sendRequest = () => {
    console.log("Add new adress");
    // Adauga adresa noua
    axios
      .post(data.baseUrl + "api/auth/add_adress", {
        token: props.token,
        adress: "New adress",
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Adauga adresa</button>;
};

export default AddAdress;
