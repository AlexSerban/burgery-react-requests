import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const GetCategories = (props) => {
  const sendRequest = () => {
    console.log("get categories");
    // Obtine categoriile
    axios.get(data.baseUrl + "api/categories").then((res) => {
      console.log(res.data);
    });
  };

  return <button onClick={sendRequest}>Get CATEGORIES</button>;
};

export default GetCategories;
