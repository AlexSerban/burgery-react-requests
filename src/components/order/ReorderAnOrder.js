import React, { useState } from "react";
import axios from "axios";
import data from "../../constants";

const ReorderAnOrder = (props) => {
  const sendRequest = () => {
    console.log("get current user's orders");
    // Comanda din nou comanda cu id-ul order_id
    const order_id = 2;

    axios
      .get(data.baseUrl + "api/auth/reorder/" + order_id, {
        params: {
          token: props.token,
        },
      })
      .then((res) => {
        console.log(res.data);
      });
  };

  return <button onClick={sendRequest}>Comanda din nou</button>;
};

export default ReorderAnOrder;
